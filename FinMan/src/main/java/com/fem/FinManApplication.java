/*---------------------------------------------------------------------
 * FinMan
 * FEM 20210609-1: Initial commit
 * FEM 20210609-2: added from Bitbucket cloud
 * 
 */

package com.fem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinManApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinManApplication.class, args);
	}

}
